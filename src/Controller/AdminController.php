<?php

/**
 * @file
 * Contains \Drupal\ddelivery\Controller\AdminController.
 */

namespace Drupal\ddelivery\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide admin for DDelivery module.
 */
class AdminController extends ControllerBase {

  /**
   * The crawler to fetch data from web service. Guzzle Client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  private $crawler;

  /**
   * Constructs a AdminController object.
   *
   * @param \GuzzleHttp\ClientInterface $crawler
   *   Guzzle Http Client.
   */
  public function __construct(ClientInterface $crawler) {
    $this->crawler = $crawler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $crawler = new Client(['base_url' => 'http://cabinet.ddelivery.ru']);
    return new static(
      $crawler
    );
  }

  /**
   * Presents the page with tests for DDelivery.
   *
   * @return array
   *   Page build array.
   */
  public function adminOverview() {
    return [
      '#type' => 'item-list',
      '@items' => [
        [
          '#type' => 'link',
          '#title' => t('DDelivery'),
          '#url' => Url::fromRoute('ddelivery.admin_overview'),
          '#options' => array(
            'attributes' => array(
              'title' => t('DDelivery'),
            ),
          ),
        ],


        [
          '#type' => 'textfield',
          '#title' => t('DDelivery'),
        ],

        [
          '#type' => 'link',
          '#title' => t('City autocomplete'),
          '#url' => Url::fromRoute('ddelivery.city_autocomplete', ['q' => 'Москв']),
          '#options' => array(
            'attributes' => array(
              'title' => t('DDelivery'),
            ),
          ),
        ],


      ],
    ];
  }

  /**
   * @param string $q
   *   City pattern.
   *
   * @return array Page build array.
   *   Page build array.
   */
  public function autocomplete($q = '') {
    $options = [
      'query' => [
        '_action' => 'autocomplete',
        'q' => $q,
      ],
      'headers' => array(
        'Content-Type' => 'application/json; charset=utf-8',
      ),
    ];
    try {
      $response = $this->crawler->get('/daemon', $options);
      $response = $response->json();
    }
    catch (BadResponseException $e) {
      // Unknown server HTTP response
      $r = $e->getResponse();
      $this->errorCode = 1;
      $this->message = $r->getStatusCode() . ' ' . $r->getReasonPhrase();
    }
    catch (RequestException $e) {
      // Network is down
      $this->errorCode = 2;
      $this->message = $e->getMessage();
    }
    catch (\RuntimeException $e) {
      // JSON error
      $this->errorCode = 3;
      $this->message = $e->getMessage();
    }

    $response = var_export($response, TRUE);

    return [
      '#markup' => $response,
    ];
  }

}
