<?php

/**
 * @file
 * Contains \Drupal\ddelivery\Component\City.
 */

namespace Drupal\ddelivery\Component;

class City extends DDeliveryObjectBase {

  private $_id;

  private $name;

  private $region;

  private $region_id;

  private $type;

  private $postal_code;

  private $area;

  private $importance;

  private $kladr;

  /**
   * @return mixed
   */
  public function getId() {
    return $this->_id;
  }

  /**
   * @param mixed $id
   */
  public function setId($id) {
    $this->_id = $id;
  }

  /**
   * @return mixed
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param mixed $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return mixed
   */
  public function getRegion() {
    return $this->region;
  }

  /**
   * @param mixed $region
   */
  public function setRegion($region) {
    $this->region = $region;
  }

  /**
   * @return mixed
   */
  public function getRegionId() {
    return $this->region_id;
  }

  /**
   * @param mixed $region_id
   */
  public function setRegionId($region_id) {
    $this->region_id = $region_id;
  }

  /**
   * @return mixed
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @param mixed $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * @return mixed
   */
  public function getPostalCode() {
    return $this->postal_code;
  }

  /**
   * @param mixed $postal_code
   */
  public function setPostalCode($postal_code) {
    $this->postal_code = $postal_code;
  }

  /**
   * @return mixed
   */
  public function getArea() {
    return $this->area;
  }

  /**
   * @param mixed $area
   */
  public function setArea($area) {
    $this->area = $area;
  }

  /**
   * @return mixed
   */
  public function getImportance() {
    return $this->importance;
  }

  /**
   * @param mixed $importance
   */
  public function setImportance($importance) {
    $this->importance = $importance;
  }

  /**
   * @return mixed
   */
  public function getKladr() {
    return $this->kladr;
  }

  /**
   * @param mixed $kladr
   */
  public function setKladr($kladr) {
    $this->kladr = $kladr;
  }

}
